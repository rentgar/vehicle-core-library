include "ai_taillightcontrolschedulecommand.gs"
include "drivercommand.gs"
include "vcl_base.gs"


final class AI_TailLightControlCommand isclass DriverCommand
{

  StringTable m_strtable;
  Menu m_menu;

  final Menu CreateLightMenu(string command)
  {
    Menu menu = Constructors.NewMenu();
    menu.AddItem(m_strtable.GetString("menu-light-top-left"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_LEFTTOP);
    menu.AddItem(m_strtable.GetString("menu-light-top-right"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_RIGHTTOP);
    menu.AddItem(m_strtable.GetString("menu-light-middle-left"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_LEFTMIDDLE);
    menu.AddItem(m_strtable.GetString("menu-light-middle-right"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_RIGHTMIDDLE);
    menu.AddItem(m_strtable.GetString("menu-light-bottom-left"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_LEFTBOTTOM);
    menu.AddItem(m_strtable.GetString("menu-light-bottom-right"), me, "AutoDriveCommand", command + ":" + VCL_TailLights.TAILLIGHT_RIGHTBOTTOM);
    return menu;
  }
  
  final Menu CreateModeMenu(string command)
  {
    Menu menu = Constructors.NewMenu();
    menu.AddItem(m_strtable.GetString("menu-mode-auto"), me, "AutoDriveCommand", command + ":automatic");
    menu.AddSeperator();
    menu.AddItem(m_strtable.GetString("menu-mode-oncargo"), me, "AutoDriveCommand", command + ":set:" + (VCL_TailLights.TAILLIGHT_LEFTMIDDLE | VCL_TailLights.TAILLIGHT_RIGHTMIDDLE));
    menu.AddItem(m_strtable.GetString("menu-mode-onpass"), me, "AutoDriveCommand", command + ":set:" + (VCL_TailLights.TAILLIGHT_LEFTTOP | VCL_TailLights.TAILLIGHT_LEFTMIDDLE | VCL_TailLights.TAILLIGHT_RIGHTMIDDLE));
    menu.AddSubmenu(m_strtable.GetString("menu-mode-on"), CreateLightMenu(command + ":on"));
    menu.AddSeperator();
    menu.AddItem(m_strtable.GetString("menu-mode-offall"), me, "AutoDriveCommand", command + ":set:0");
    menu.AddSubmenu(m_strtable.GetString("menu-mode-off"), CreateLightMenu(command + ":off"));
    return menu;
  }
  
  final Menu CreateSideMenu(string command)
  {
    Menu menu = Constructors.NewMenu();
    menu.AddSubmenu(m_strtable.GetString("menu-side-front"), CreateModeMenu(command + ":front"));
    menu.AddSubmenu(m_strtable.GetString("menu-side-back"), CreateModeMenu(command + ":back"));
    return menu;
  }

  final Menu CreateTrainSideMenu(string command)
  {
    Menu menu = Constructors.NewMenu();
    menu.AddSubmenu(m_strtable.GetString("menu-side-fromfront"), CreateSideMenu(command + ":front"));
    menu.AddSubmenu(m_strtable.GetString("menu-side-fromback"), CreateSideMenu(command + ":back"));
    return menu;
  }
  
  final Menu CreatePositionMenu(string command)
  {
    Menu menu = Constructors.NewMenu();
    int i,j;
    for(i = 0; i < 10; ++i) {
      j = 0;
      if(i == 0) j = 1;
      Menu submenu = Constructors.NewMenu();
      for(; j < 10; ++j) submenu.AddSubmenu(m_strtable.GetString1("menu-number", i * 10 + j), CreateTrainSideMenu(command + ":" + (i * 10 + j - 1)));
      menu.AddSubmenu(m_strtable.GetString1("menu-decnumber", i), submenu);
    }
    return menu;
  }
  
  final void CreateMenu()
  {
    m_menu = Constructors.NewMenu();
    m_menu.AddSubmenu(m_strtable.GetString("menu-train-side-front"), CreateModeMenu("head"));
    m_menu.AddSubmenu(m_strtable.GetString("menu-train-side-back"), CreateModeMenu("tail"));
    m_menu.AddSeperator();
    m_menu.AddSubmenu(m_strtable.GetString("menu-wagon"), CreatePositionMenu("wagon"));
  }
  
  public void Init(Asset asset)
  {
		inherited(asset);
		me.AddHandler(me, "AutoDriveCommand", "", "MenuHandler");
		m_strtable = asset.GetStringTable();
    CreateMenu();
  }
  
  public void AddCommandMenuItem(DriverCharacter driver, Menu menu)
  {
    menu.AddSubmenu(m_strtable.GetString("menu-control"), m_menu);
  }
  
  DriverScheduleCommand CreateScheduleCommand(DriverCharacter driver, Soup soup)
	{
		AI_TailLightControlScheduleCommand cmd = new AI_TailLightControlScheduleCommand();
		cmd.Init(driver, me);
		cmd.SetProperties(soup);
    return cast<DriverScheduleCommand>cmd;
	}
 
  
  final void MenuHandler(Message msg)
  {
    string[] args = Str.Tokens(msg.minor, ":");
    int target = 0;
    int targetindex = -1;
    int light = 0;
    if(args[0] == "head") target = 1;
    else if(args[0] == "tail") target = 2;
    if(target == 0) { 
      targetindex = Str.ToInt(args[1]);
      if(args[2] == "front") target = target | 4;
      if(args[3] == "front") target = target | 8;
      if(args[4] == "on" or args[4] == "automatic") target = target | 16;
      if(args[4] == "set" or args[4] == "automatic") target = target | 32;
      if(args[4] != "automatic") light = Str.ToInt(args[5]); 
    } else {
      if(args[1] == "on" or args[1] == "automatic") target = target | 16;
      if(args[1] == "set" or args[1] == "automatic") target = target | 32;
      if(args[1] != "automatic") light = Str.ToInt(args[2]); 
    }
		DriverCharacter driver = cast<DriverCharacter>(msg.src);
	  DriverCommands commands = me.GetDriverCommands(msg);
  	Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Target", target);
    soup.SetNamedTag("Lights", light);
    if(targetindex >= 0) soup.SetNamedTag("TargetIndex", targetindex);
		commands.AddDriverScheduleCommand(me.CreateScheduleCommand(driver, soup));
  }



};