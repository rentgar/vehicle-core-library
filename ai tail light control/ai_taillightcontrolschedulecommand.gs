include "driverschedulecommand.gs"
include "ecl_bitlogic.gs"             //Extended Class Library
include "vcl_base.gs"                 //Vehicle Class Library

final class AI_TailLightControlCustomCommand isclass CustomCommand
{

  int m_target, m_targetindex, m_light;
  
  public AI_TailLightControlCustomCommand Init(int target, int targetIndex, int light)
  {
    m_target = target;
    m_targetindex = targetIndex;
    m_light = light;
    return me; 
  }

  public bool Execute(Train train, int px, int py, int pz)
  {
    Vehicle[] vehs = train.GetVehicles();
    int index;
    if(m_target & 1) index = 0;
    else if(m_target & 2) index = vehs.size() - 1;
    else if(m_target & 4) index = m_targetindex;
    else index = vehs.size() - m_targetindex - 1;
    
    Interface.Print(m_target + " " + m_targetindex + " " + m_light + " " + index + " " + vehs.size());
    
    if(index < 0 or index >= vehs.size()) return false; 
    VCL_TailLights controller = cast<VCL_TailLights>((object)vehs[index]);
    if(!controller) return false;
    bool front = (vehs[index].GetDirectionRelativeToTrain() and (m_target & 1 or (!(m_target & (1 | 2)) and m_target & 8))) or 
                 (!vehs[index].GetDirectionRelativeToTrain() and (m_target & 2 or (!(m_target & (1 | 2)) and !(m_target & 8))));
    if(ECLLogic.GetBitMask(m_target, 16 | 32)) controller.VCLTLSetAutomaticControl(front);
    else if(m_target & 32) controller.VCLTLSetLightStatus(front, m_light);
    else if(m_target & 16) controller.VCLTLSetLightOn(front, m_light);
    else controller.VCLTLSetLightOff(front, m_light);
    return true;
  }
  
  public bool ShouldStopTrainOnCompletion() { return false; }  

};



final class AI_TailLightControlScheduleCommand isclass DriverScheduleCommand
{

  int m_target, m_targetindex, m_light;

	public bool BeginExecute(DriverCharacter driver)
	{
		Train train = driver.GetTrain();
		if(!train)return false;
    AI_TailLightControlCustomCommand command = new AI_TailLightControlCustomCommand().Init(m_target, m_targetindex, m_light);
		driver.DriverCustomCommand(command);
		driver.DriverIssueSchedule();
		return true;
	}

	public object GetIcon(void)
  {
    return (object)(me.GetDriverCommand());
  }

  public string GetTooltip(void) 
  {
		StringTable strtable = me.GetAsset().GetStringTable();
    string tooltip;
    if(ECLLogic.GetBitMask(m_target, 16 | 32)) tooltip = strtable.GetString("tooltip-automatic");
    else if(m_target & 32 and ECLLogic.GetBitMask(m_light, VCL_TailLights.TAILLIGHT_LEFTTOP | VCL_TailLights.TAILLIGHT_LEFTMIDDLE | VCL_TailLights.TAILLIGHT_RIGHTMIDDLE)) tooltip = strtable.GetString("tooltip-set-pass");
    else if(m_target & 32 and ECLLogic.GetBitMask(m_light, VCL_TailLights.TAILLIGHT_LEFTMIDDLE | VCL_TailLights.TAILLIGHT_RIGHTMIDDLE)) tooltip = strtable.GetString("tooltip-set-cargo");
    else if(m_target & 32 and m_light == 0) tooltip = strtable.GetString("tooltip-offall");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_LEFTTOP) tooltip = strtable.GetString("tooltip-on-top-left");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_RIGHTTOP) tooltip = strtable.GetString("tooltip-on-top-right");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_LEFTMIDDLE) tooltip = strtable.GetString("tooltip-on-middle-left");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_RIGHTMIDDLE) tooltip = strtable.GetString("tooltip-on-middle-right");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_LEFTBOTTOM) tooltip = strtable.GetString("tooltip-on-bottom-left");
    else if(m_target & 16 and m_light == VCL_TailLights.TAILLIGHT_RIGHTBOTTOM) tooltip = strtable.GetString("tooltip-on-bottom-right");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_LEFTTOP) tooltip = strtable.GetString("tooltip-off-top-left");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_RIGHTTOP) tooltip = strtable.GetString("tooltip-off-top-right");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_LEFTMIDDLE) tooltip = strtable.GetString("tooltip-off-middle-left");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_RIGHTMIDDLE) tooltip = strtable.GetString("tooltip-off-middle-right");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_LEFTBOTTOM) tooltip = strtable.GetString("tooltip-off-bottom-left");
    else if(!(m_target & (16 | 32)) and m_light == VCL_TailLights.TAILLIGHT_RIGHTBOTTOM) tooltip = strtable.GetString("tooltip-off-bottom-right");
    if(m_target & 1) return tooltip + " " + strtable.GetString("tooltip-train-side-front");
    else if(m_target & 2) return tooltip + " " + strtable.GetString("tooltip-train-side-back");
    else if(m_target & 4) tooltip = tooltip + " " + strtable.GetString1("tooltip-wagon-head", m_targetindex + 1);
    else tooltip = tooltip + " " + strtable.GetString1("tooltip-wagon-tail", m_targetindex + 1);
    if(m_target & 8) tooltip = tooltip + " " + strtable.GetString("tooltip-side-front");
    else tooltip = tooltip + " " + strtable.GetString("tooltip-side-back");     
    return tooltip; 
  }

  public Soup GetProperties(void) 
  { 
    Soup soup = inherited();
    soup.SetNamedTag("Target", m_target);
    soup.SetNamedTag("Lights", m_light);
    if(m_targetindex >= 0) soup.SetNamedTag("TargetIndex", m_targetindex);
    return soup; 
  }

  public void SetProperties(Soup soup) 
  {
    m_target = soup.GetNamedTagAsInt("Target");
    m_targetindex = soup.GetNamedTagAsInt("TargetIndex", -1);
    m_light = soup.GetNamedTagAsInt("Lights");
  }

};