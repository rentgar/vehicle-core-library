include "vcl_base.gs"
include "library.gs"

class VCL_Core isclass Library
{


  public bool VCLCSetDayAndNightTime(float startDayTime, float startNightTime);

  public bool VCLCIsNight(void);



	//
	// РЕАЛИЗАЦИЯ
	//
  
  float m_daytime                       = 0.25;                       //Время начала дня
  float m_nighttime                     = 0.75;                       //Время начала ночи
  bool m_isnight                        = false;                      //Текущее состояние индикатора ночи
  VCL_TailLights[] m_taillights         = new VCL_TailLights[0];      //Коллекция единиц с контролем хвостовых сигналов  
  VCL_BrakeFixture[] m_brakefixtures    = new VCL_BrakeFixture[0];    //Коллекция единиц с контролем тормозной арматурой


  public void VCLCAddTailLight(VCL_TailLights tailLight)
  {
    if(tailLight and tailLight.VCLTLGetMyVehicle()) {
      m_taillights[m_taillights.size()] = tailLight;
      if(m_isnight) tailLight.VCLTLUpdateTailLight();   
    }
  }

  //Пытается обновить хвостовые огни для указаной подвижной единицы  
  final void VCLCTryUpdateTailLight(Vehicle veh)
  {
    VCL_TailLights taillight = cast<VCL_TailLights>((object)veh);
    if(taillight) {
      taillight.VCLTLUpdateTailLight();
    } 
  }

  
  public void VCLCAddBrakeFixture(VCL_BrakeFixture brakefixture)
  {
    if(brakefixture and brakefixture.VCLBFGetMyVehicle()) {
      m_brakefixtures[m_brakefixtures.size()] = brakefixture;
      brakefixture.VCLBFUpdateBrakeFixture();
    }
  }

  public bool VCLCSetDayAndNightTime(float startDayTime, float startNightTime)
  {
    if(startDayTime < 0.0 or startDayTime > 1.0) { Exception("Argument \"startDayTime\" has invalid value"); return false; }
    if(startNightTime < 0.0 or startNightTime > 1.0) { Exception("Argument \"startNightTime\" has invalid value"); return false; }
    if(startDayTime > startNightTime) { Exception("Value of argument \"startDayTime\" cannot be greater than value of argument \"startNightTime\""); return false; }
    m_daytime = startDayTime;
    m_nighttime = startNightTime;  
    return true;
  }

  public bool VCLCIsNight(void)
  {
    float gametime = World.GetGameTime();
    gametime = gametime + 0.5;
    if(gametime >= 1.0) gametime = gametime - 1.0;
    return gametime < m_daytime or gametime >= m_nighttime; 
  }
  
  final thread void VCLCTimeOfDayControlLoop(void)
  {
    while(true) {
      bool isnight = VCLCIsNight();
      if(isnight != m_isnight) { 
        m_isnight = isnight;
        int index = 0;
        int counter = 0;
        while(index < m_taillights.size()) {
          if(m_taillights[index].VCLTLGetMyVehicle())  m_taillights[index++].VCLTLUpdateTailLight();
          else m_taillights[index, index + 1] = null;
          if(++counter == 20) {
            counter = 0;
            Sleep(0.001);
          }   
        }
      } else Sleep(1.0);
    }
  } 
  
  final thread void VCLCBrakeFixureControlLooper()
  {
    int index = 0;
    int counter = 0;
    while(true) {
      if(index < m_brakefixtures.size()) {
        m_brakefixtures[index++].VCLBFUpdateBrakeFixture();
        if(index >= m_brakefixtures.size()) index = 0;
      } else Sleep(1.0);
      if(++counter == 20) {
        counter = 0;
        Sleep(0.001);
      }   
    }
  }
  
  


  final thread void VCLCInitCoupleItems(void)
  {
    AsyncObjectSearchResult result = World.GetNamedObjectList(AssetCategory.Consist, "");
    if(result.SynchronouslyWaitForResults()) {
      NamedObjectInfo[] items = result.GetResults();
      int i, j;
      for(i = 0; i < items.size(); ++i) {
        Train train = cast<Train>items[i].objectRef;
        if(train) {
          Vehicle[] trainvehs = train.GetVehicles();
          for(j = 0; j < trainvehs.size(); ++j) {
            VCL_CoupleItem currcoupleitem = cast<VCL_CoupleItem>((object)trainvehs[j]);
            if(currcoupleitem and !currcoupleitem.VCLCIIsInited()) {
              if(j > 0) currcoupleitem.VCLCIInitState(trainvehs[j].GetDirectionRelativeToTrain(), trainvehs[j - 1]); 
              else currcoupleitem.VCLCIInitState(trainvehs[j].GetDirectionRelativeToTrain(), null);
              if(j < trainvehs.size() - 1) currcoupleitem.VCLCIInitState(!trainvehs[j].GetDirectionRelativeToTrain(), trainvehs[j + 1]); 
              else currcoupleitem.VCLCIInitState(!trainvehs[j].GetDirectionRelativeToTrain(), null);
              currcoupleitem.VCLCISetInitedState();
            }
          }
          VCLCTryUpdateTailLight(trainvehs[0]);
          if(trainvehs.size() > 1) VCLCTryUpdateTailLight(trainvehs[trainvehs.size() - 1]);
          Sleep(0.001);
        }
      }
    }
  }











  final void VCLCModuleHandler(Message msg)
  {
    if(World.GetCurrentModule() == World.DRIVER_MODULE){
      //me.AddHandler(me, "Vehicle", "Coupled", "VCLCCoupleHandler");
      //me.AddHandler(me, "Vehicle", "Decoupled", "VCLCDecoupleHandler");
      VCLCTimeOfDayControlLoop();
      VCLCBrakeFixureControlLooper();
      VCLCInitCoupleItems();
    }
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    me.AddHandler(me, "World", "ModuleInit", "VCLCModuleHandler");
  }
  
  
  
  
  
  
  

};