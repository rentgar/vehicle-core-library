//=============================================================================
// File: vcl_base.gs
// Desc: Содержит набор основных базовых классов интерфейсов
// Auth: Алексей 'Эрендир' Зверев 2019 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "vehiclecoupleprovider.gs"    //EPT Core library
include "ecl_bitlogic.gs"             //Extended Class Library
include "vcl_coupleitem.gs"
include "vcl_core.gs"

//=============================================================================
// Name: VCL_VehicleWithTurnbuckle
// Desc: Класс, реализующий работу винтовой стяжки на подвижном составе
// Note: Для работы необходим создать актив с kind mesh. В нём должны быть
//       в mesh-table настроены две меши с именами coupled - соединённая
//       сцепка, и decoupled - разъеденённая сцепка. 
//       Пример:
//       mesh-table
//       {
//         coupled
//         {
//           mesh                                "coupledmesh.lm"
//           auto-create                         0
//         }
//         decoupled
//         {
//           mesh                                "decoupledmesh.lm"
//           auto-create                         1
//         }
//       }
//       В конфигурационном файле вагона должны быть два attachment эффекта
//       с подключением mesh актива с мешами сцепок coupler-front для 
//       передней сцепки и coupler-back для задней. Пример:
//       effects
//       {
//         coupler-front
//         {
//           kind                            "attachment"
//           att                             "a.limfront"
//           default-mesh                    <kuid:xxxxxx:yyyyyy>
//         }
  
//         coupler-back
//         {
//           kind                            "attachment"
//           att                             "a.limback"
//           default-mesh                    <kuid:xxxxxx:yyyyyy>
//         }
//       }
//
//       Класс расширяет функционал VCL_CoupleItem и не может одновременно
//       наследоваться с ним. 
//
// See also: 
//       VCL_CoupleItem
//=============================================================================
class VCL_VehicleWithTurnbuckle isclass VCL_CoupleItem
{

  //=============================================================================
  // Name: COUPLERSTATE_*
  // Desc: Текущее состояние сцепного устройства
  //=============================================================================
  define int COUPLERSTATE_UNDEFINED   = 0;    //Сцепное устройство расцеплено, состояние не определено
  define int COUPLERSTATE_DECOUPLE    = 1;    //Сцепное устройство расцеплено 
  define int COUPLERSTATE_COUPLE      = 2;    //Сцепное устройство сцеплено

  //=============================================================================
  // Name: VCLCIOnCoupleEvent
  // Desc: Событие сцепки/расцепки
  // See:  VCL_CoupleItem.VCLCIOnCoupleEvent(int, int, Vehicle)
  //=============================================================================
  mandatory void VCLCIOnCoupleEvent(int event, int side, Vehicle neighbour);

  //=============================================================================
  // Name: VCLVWTGetProperties
  // Desc: Возвращает Soup с сохранённым состояним сцепных устройств.
  //=============================================================================
  mandatory Soup VCLVWTGetProperties(void);

  //=============================================================================
  // Name: VCLVWTSetProperties
  // Desc: Восстанавливает сохранённе состояние сцепных устройств
  // Parm: soup - Объект Soup, хранящий состояние сцепных устройств.
  //=============================================================================
  mandatory void VCLVWTSetProperties(Soup soup);

  //=============================================================================
  // Name: VCLVWTGetCouplerState
  // Desc: Возвращает значение одной из констант COUPLERSTATE_*, определяющее
  //       текущее состояние сцепного устройства 
  // Parm: front - значение true, для получения состояния переднего сцепного 
  //       устройства, или значение false, для получения состояния заднего
  //       сцепного устройства
  //=============================================================================
  final int VCLVWTGetCouplerState(bool front);

  //=============================================================================
  // Name: VCLVWTGetCouplerStateOfNeighbor
  // Desc: Возвращает значение одной из констант COUPLERSTATE_*, указывающее
  //       состояние сцепного устройства, для сцепки с указанной подвижной
  //       еденицей
  // Parm: neighbor - Объект Vehicle, состояние сцепки с которым нужно получить
  //       состояние.
  //=============================================================================
  final int VCLVWTGetCouplerStateOfNeighbor(Vehicle neighbor); 

	//
	// РЕАЛИЗАЦИЯ
	//

  int _frontcouplerstate    = COUPLERSTATE_UNDEFINED;       //Состояние передней сцепки
  int _backcouplerstate     = COUPLERSTATE_UNDEFINED;       //Состояние задней сцепки

  //Обновляет состояние сцепного устройства
  //front - true спереди; false сзади
  //newState - COUPLERSTATE_* с новым состоянием 
  final void UpdateCoupler(bool front, int newState)
  {
    if((front and newState != _frontcouplerstate) or (!front and newState != _backcouplerstate)) {
      bool oldcouple = (front and _frontcouplerstate == COUPLERSTATE_COUPLE) or (!front and _backcouplerstate == COUPLERSTATE_COUPLE);
      if(oldcouple != (bool)(newState == COUPLERSTATE_COUPLE)) {
        if(newState == COUPLERSTATE_COUPLE){
          if(front) _myveh.GetFXAttachment("coupler-front").SetMesh("coupled", 0.0);
          else _myveh.GetFXAttachment("coupler-back").SetMesh("coupled", 0.0);
        } else {
          if(front) _myveh.GetFXAttachment("coupler-front").SetMesh("decoupled", 0.0);
          else _myveh.GetFXAttachment("coupler-back").SetMesh("decoupled", 0.0);
        }
      }
      if(front) _frontcouplerstate = newState;
      else _backcouplerstate = newState;
    }
  }

  mandatory void VCLCIOnCoupleEvent(int event, int side, Vehicle neighbour)
  {
    if(event & COUPLEEVENT_COUPLE) { //Если произошла сцепка
      VCL_VehicleWithTurnbuckle turnbuckleveh = cast<VCL_VehicleWithTurnbuckle>((object)neighbour);
      if(turnbuckleveh) {
        int state = turnbuckleveh.VCLVWTGetCouplerStateOfNeighbor(_myveh);
        int newstate = COUPLERSTATE_DECOUPLE;
        if(state == COUPLERSTATE_DECOUPLE or (state == COUPLERSTATE_UNDEFINED and Math.Rand(0, 100000) % 2 == 0)) newstate = COUPLERSTATE_COUPLE;
        UpdateCoupler(side == COUPLESIDE_FRONT, newstate);  
        return;
      }
    }
    UpdateCoupler(side == COUPLESIDE_FRONT, COUPLERSTATE_UNDEFINED);
  }

  mandatory Soup VCLVWTGetProperties(void)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("FrontCouplerState", _frontcouplerstate);
    soup.SetNamedTag("BackCouplerState", _backcouplerstate);
    return soup;
  }

  mandatory void VCLVWTSetProperties(Soup soup)
  {
    UpdateCoupler(true, soup.GetNamedTagAsInt("FrontCouplerState", _frontcouplerstate));
    UpdateCoupler(false, soup.GetNamedTagAsInt("BackCouplerState", _backcouplerstate));
  }

  final int VCLVWTGetCouplerState(bool front)
  {
    if(front) return _frontcouplerstate;
    return _backcouplerstate;
  }

  final int VCLVWTGetCouplerStateOfNeighbor(Vehicle neighbor) 
  { 
    if(neighbor == _frontveh) return _frontcouplerstate;
    else if(neighbor == _rearveh) return _backcouplerstate;
    return COUPLERSTATE_UNDEFINED;
  } 

  mandatory void VCLCIInit(void)
  {
    inherited();
    Asset libraryasset = _myveh.GetAsset().FindAsset("vehicle-class-library");
    if(!libraryasset) return;
    if(libraryasset == _frontcouplesound) {
      _frontcouplesoundfile = "sounds/turnbuckle_couple.wav"; 
      _frontdecouplesoundfile = "sounds/turnbuckle_decouple.wav"; 
    }
    if(libraryasset == _rearcouplesound) {
      _rearcouplesoundfile = "sounds/turnbuckle_couple.wav"; 
      _reardecouplesoundfile = "sounds/turnbuckle_decouple.wav"; 
    }
  }

};

//=============================================================================
// Name: VCL_VehicleWithTurnbuckle
// Desc: Класс, реализующий работу хвостовых огней на вагонах
// Note: Это класс является абстрактным и для работы должен быть унаследован.
//       Класс наследник должен наследоваться от Vehicle.
//       Может быть использован только совместно с VCL_CoupleItem или
//       VCL_VehicleWithTurnbuckle.
//
//       Хвостовые огни должны быть прописаны в mesh-table с заданными именами
//       конкретных сеток.
//       Список имён сеток для хвостовых огней:
//
//       taillight-front-top-left — Верхний левый огонь спереди вагона. 
//         Использует Используется на пассажирских вагонах.
//       Направление взгляда: → [===]
//
//       taillight-front-top-right — Верхний правый огонь спереди вагона.
//         Использует Используется на пассажирских вагонах.
//       Направление взгляда: → [===]
//
//       taillight-front-back-left — Верхний левый огонь сзади вагона. 
//         Использует Используется на пассажирских вагонах.
//       Направление взгляда: [===] ←
//
//       taillight-front-back-right — Верхний правый огонь сзади вагона.
//         Использует Используется на пассажирских вагонах.
//       Направление взгляда: [===] ←
//
//      taillight-front-middle-left — Нижний левый огонь спереди вагона.
//        Левый катафот на европейских вагонах
//      Направление взгляда: → [===]
//
//      taillight-front-middle-right — Нижний правый ононь спереди вагона.
//        Правый катафот на европейских вагонах, крастный диск на грузовых
//        вагонах, нижний огонь на пассажирских.
//      Направление взгляда: → [===]
//
//      taillight-back-middle-left — Нижний левый огонь сзади вагона.
//        Левый катафот на европейских вагонах
//       Направление взгляда: [===] ←
//
//      taillight-back-middle-right — Нижний правый ононь сзади вагона.
//        Правый катафот на европейских вагонах, крастный диск на грузовых
//        вагонах, нижний огонь на пассажирских.
//       Направление взгляда: [===] ←
//
//      taillight-front-bottom-left — Нижний левый огонь спереди вагона
//        Дополнительный нижной буфер ниже буферного бруса, например фонарь или
//        флажок.
//      Направление взгляда: → [===]
//
//      taillight-front-bottom-right — Нижний правый огонь спереди вагона
//        Дополнительный нижной буфер ниже буферного бруса, например фонарь или
//        флажок.
//      Направление взгляда: → [===]
//
//      taillight-back-bottom-left — Нижний левый огонь сзади вагона
//        Дополнительный нижной буфер ниже буферного бруса, например фонарь или
//        флажок.
//       Направление взгляда: [===] ←
//
//      taillight-back-bottom-right — Нижний правый огонь сзади вагона
//        Дополнительный нижной буфер ниже буферного бруса, например фонарь или
//        флажок.
//       Направление взгляда: [===] ←
//
//      Должны быть приписаны только те сетки, которые используются на вагоне.
//      Все сетки должны быть по умолчанию скрыты (auto-create 0)
//=============================================================================
class VCL_TailLights
{

  //=============================================================================
  // Name: TAILLIGHT_*
  // Desc: Положение хвостового огня
  //=============================================================================
  public define int TAILLIGHT_LEFTBOTTOM     = 1 << 0;   //Огонь слева снизу
  public define int TAILLIGHT_RIGHTBOTTOM    = 1 << 1;   //Огонь справа снизу 
  public define int TAILLIGHT_LEFTMIDDLE     = 1 << 2;   //Огонь слева в середине
  public define int TAILLIGHT_RIGHTMIDDLE    = 1 << 3;   //Огонь справа в середине 
  public define int TAILLIGHT_LEFTTOP        = 1 << 4;   //Огонь слева сверху 
  public define int TAILLIGHT_RIGHTTOP       = 1 << 5;   //Огонь справа сверху 

  //=============================================================================
  // Name: VCLTLInit
  // Desc: Выполняет начальную инициализацию класса.
  // Note: Необходимо обязательно вызывать в функции void Init(Asset) класса
  //       наследника. Класс наследник должен наследоваться от Vehicle и 
  //       VCL_CoupleItem или VCL_VehicleWithTurnbuckle
  //=============================================================================
  final void VCLTLInit(void);

  //=============================================================================
  // Name: VCLTLGetMyVehicle
  // Desc: Возвращает объект Vehicle, который реализует хвостовые огни.
  //=============================================================================
  public final Vehicle VCLTLGetMyVehicle(void);

  //=============================================================================
  // Name: VCLTLGetMyVehicle
  // Desc: Возвращает значение true, если в данный момент ночь; в противном
  //       случае — значение false. 
  // Note: Данные берутся из библиотеки VCL_Core.
  //=============================================================================
  final bool VCLTLIsNight(void);

  //=============================================================================
  // Name: VCLTLGetLightStatus
  // Desc: Возвращает маску текущих включённых хвостовых огней для указанной 
  //       стороны.
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Retn: Побитовая маска значений констант TAILLIGHT_*, определяющая текущие 
  //       включённые хвостовые огни.
  //=============================================================================
  public final int VCLTLGetLightStatus(bool front);

  //=============================================================================
  // Name: VCLTLSetLightStatus
  // Desc: Задаёт маску текущих включённых хвостовых огней для указанной 
  //       стороны.
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Parm: lightOn — Побитовая маска значений констант TAILLIGHT_*, определяющая 
  //       хвостовые огни, которые необходимо включить.  
  //=============================================================================
  public final void VCLTLSetLightStatus(bool front, int lightOn);

  //=============================================================================
  // Name: VCLTLSetLightOn
  // Desc: Включает указанные хвостовые огни, не изменяя состояние неуказанных.
  // Parm: front - Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае - значение false.
  // Parm: lightOn - Побитовая маска значений констант TAILLIGHT_*, определяющая 
  //       хвостовые огни, которые необходимо включить.  
  //=============================================================================
  public final void VCLTLSetLightOn(bool front, int lights);

  //=============================================================================
  // Name: VCLTLSetLightOff
  // Desc: Выключает указанные хвостовые огни, не изменяя состояние неуказанных
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false
  // Parm: lightOn — Побитовая маска значений констант TAILLIGHT_*, определяющая 
  //       хвостовые огни, которые необходимо выключить  
  //=============================================================================
  public final void VCLTLSetLightOff(bool front, int lights);

  //=============================================================================
  // Name: VCLTLIsAutomaticControl
  // Desc: Возвращает значение true, если хвостовые огни с указанной стороны 
  //       управляются автоматически.
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  //=============================================================================
  public final bool VCLTLIsAutomaticControl(bool front);

  //=============================================================================
  // Name: VCLTLSetAutomaticControl
  // Desc: Включает автоматическое управление хвостовыми огнями с указанной
  //       стороны.
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  //=============================================================================
  public final void VCLTLSetAutomaticControl(bool front);

  //=============================================================================
  // Name: VCLTLGetAvailableLights
  // Desc: Возвращает маску доступных хвостовых огней для указанной стороны
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Retn: Побитовая маска значений констант TAILLIGHT_*, определяющая доступные 
  //       хвостовые сигналы.
  // Note: Необходимо переопределить в производном классе при реализации 
  //       собственной логики управления отображением огней. 
  //=============================================================================
  public int VCLTLGetAvailableLights(bool front);

  //=============================================================================
  // Name: VCLTLDeterminingLigts
  // Desc: Определяет необходимое состояние хвостовых огней с указанной стороны
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Note: Необходимо переопределить в наследуемом классе для реализации 
  //       собственной логики работы хвостовых огней.
  //=============================================================================
  int VCLTLDeterminingLigts(bool front);                    

  //=============================================================================
  // Name: VCLTLLightsUpdated
  // Desc: Обновляет показание хвостовых огней
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Parm: newLightsOn — Побитовая маска значений констант TAILLIGHT_*, 
  //       определяющая хвостовые огни, которые должны отображаться.  
  // Note: Вызывается автоматически при необходимости изменения состояния огней. 
  //       Переопределить в наследуемом классе, для изменения логики отображения
  //       хвостовых огней.
  //=============================================================================
  void VCLTLLightsUpdated(bool front, int newLightsOn);

  //=============================================================================
  // Name: VCLTLGetTailLightDuration
  // Desc: Возвращает время в секундах, за которое хвостовые огни должны изменить
  //       своё состояние.
  // Parm: front — Значение true, если передняя сторона подвижной единицы; в  
  //       противном случае — значение false.
  // Parm: light — Значение одной из констант TAILLIGHT_*, определяющее для  
  //       какого огня нужно получить время смены сигнала.  
  // Note: По умолчанию возвращает 0. Для реализации необходимо переопределить
  //       в наследующем классе.
  //=============================================================================
  float VCLTLGetTailLightDuration(bool front, int light);

  //=============================================================================
  // Name: VCLTLGetProperties
  // Desc: Возвращает Soup с сохранённым состоянием хвостовых огней.
  //=============================================================================
   mandatory Soup VCLTLGetProperties(void);

  //=============================================================================
  // Name: VCLTLSetProperties
  // Desc: Восстанавливает сохранённое состояние хвостовых огней.
  // Parm: soup — Объект Soup, хранящий состояние хвостовых огней.
  //=============================================================================
   mandatory void VCLTLSetProperties(Soup soup);

	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int TAILLIGHT_FRONTAUTOCONTROL      = 1 << 24;     //Автоконтроль огней спереди   
  define int TAILLIGHT_BACKAUTOCONTROL       = 1 << 25;     //Автоконтроль сзади
  define int TAILLIGHT_AVAILABLELOADED       = 1 << 26;     //Указатель, что маска доступных огней загружена
  define int TAILLIGHT_LIGHTMASK             = TAILLIGHT_LEFTBOTTOM | TAILLIGHT_RIGHTBOTTOM | TAILLIGHT_LEFTMIDDLE |
                                               TAILLIGHT_RIGHTMIDDLE | TAILLIGHT_LEFTTOP | TAILLIGHT_RIGHTTOP;        //Маска огней      

  Vehicle _myveh;
  VCL_Core _core;
  int _lightstatus = TAILLIGHT_FRONTAUTOCONTROL | TAILLIGHT_BACKAUTOCONTROL;

  //Инициализация
  final void VCLTLInit(void) 
  {
    if(_myveh) return;
    _myveh = cast<Vehicle>((object)me);
    if(!_myveh or !_myveh.isclass(VCL_CoupleItem)){
      Interface.Exception("VCL_TailLights.VCLTLInit> implementing class is not an inheritor of Vehicle class or VCL_CoupleItem class");
      return;
    }
    Asset libraryasset = _myveh.GetAsset().FindAsset("vehicle-class-library");
    if(!libraryasset) {
      _myveh.Exception("VCL_TailLights.VCLTLInit> 'vehicle-class-library' entry in kuid-table for library asset in '" + _myveh.GetAsset().GetLocalisedName() + "' asset was not found.");
      return;
    }
    _core = cast<VCL_Core>(TrainzScript.GetLibrary(libraryasset.GetKUID()));
    _core.VCLCAddTailLight(me);
  }

  public final Vehicle VCLTLGetMyVehicle(void) 
  { 
    return _myveh; 
  }
  
  final bool VCLTLIsNight(void) 
  { 
    return _core.VCLCIsNight(); 
  }

  //Внутренняя функция. Не вызывать из стороннего скрипта
  final void VCLTLSetTailLight(bool front, int lightOn)
  {
    lightOn = lightOn & TAILLIGHT_LIGHTMASK & VCLTLGetAvailableLights(front);
    int currlight = VCLTLGetLightStatus(front);
    if(lightOn ^ currlight) {
      int setlight = lightOn;
      int mask = TAILLIGHT_LIGHTMASK;
      if(front) {
        setlight = setlight << 6;
        mask = mask << 6;
      }
      _lightstatus = (_lightstatus & ~mask) | setlight; 
      VCLTLLightsUpdated(front, lightOn);
    }
  }

  //Внутренняя функция. Не вызывать из стороннего скрипта
  public final void VCLTLUpdateTailLight(void)
  {
    if(VCLTLIsAutomaticControl(true)) VCLTLSetTailLight(true, VCLTLDeterminingLigts(true));
    if(VCLTLIsAutomaticControl(false)) VCLTLSetTailLight(false, VCLTLDeterminingLigts(false));
  } 

  public final int VCLTLGetLightStatus(bool front)
  {
    if(front) return (_lightstatus >> 6) & TAILLIGHT_LIGHTMASK;
    return _lightstatus & TAILLIGHT_LIGHTMASK;
  }

  public final void VCLTLSetLightStatus(bool front, int lightOn) 
  {
    if(front) _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_FRONTAUTOCONTROL, false);
    else _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_BACKAUTOCONTROL, false);  
    VCLTLSetTailLight(front, lightOn);
  }

  public final void VCLTLSetLightOn(bool front, int lights)  
  {
    if(front) _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_FRONTAUTOCONTROL, false);
    else _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_BACKAUTOCONTROL, false);  
    int currlight = VCLTLGetLightStatus(front);
    VCLTLSetTailLight(front, ECLLogic.SetBitMask(currlight, lights & TAILLIGHT_LIGHTMASK, true));
  }

  public final void VCLTLSetLightOff(bool front, int lights)
  {
    if(front) _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_FRONTAUTOCONTROL, false);
    else _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_BACKAUTOCONTROL, false);  
    int currlight = VCLTLGetLightStatus(front);
    VCLTLSetTailLight(front, ECLLogic.SetBitMask(currlight, lights & TAILLIGHT_LIGHTMASK, false));
  }

  public final bool VCLTLIsAutomaticControl(bool front)
  {
    if(front) return (bool)(_lightstatus & TAILLIGHT_FRONTAUTOCONTROL);
    return (bool)(_lightstatus & TAILLIGHT_BACKAUTOCONTROL);
  }
  
  public final void VCLTLSetAutomaticControl(bool front)
  {
    if(front) _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_FRONTAUTOCONTROL, true);
    else _lightstatus = ECLLogic.SetBitMask(_lightstatus, TAILLIGHT_BACKAUTOCONTROL, true);
    VCLTLUpdateTailLight();  
  }
  
  public int VCLTLGetAvailableLights(bool front)
  {
    if(!(_lightstatus & TAILLIGHT_AVAILABLELOADED)) {
      int availablelights = 0;
      if(_myveh.HasMesh("taillight-front-top-left")) availablelights = availablelights | TAILLIGHT_LEFTTOP;
      if(_myveh.HasMesh("taillight-front-top-right")) availablelights = availablelights | TAILLIGHT_RIGHTTOP;
      if(_myveh.HasMesh("taillight-front-middle-left")) availablelights = availablelights | TAILLIGHT_LEFTMIDDLE;
      if(_myveh.HasMesh("taillight-front-middle-right")) availablelights = availablelights | TAILLIGHT_RIGHTMIDDLE;
      if(_myveh.HasMesh("taillight-front-bottom-left")) availablelights = availablelights | TAILLIGHT_LEFTBOTTOM;
      if(_myveh.HasMesh("taillight-front-bottom-right")) availablelights = availablelights | TAILLIGHT_RIGHTBOTTOM;
      availablelights = availablelights << 6;
      if(_myveh.HasMesh("taillight-back-top-left")) availablelights = availablelights | TAILLIGHT_LEFTTOP;
      if(_myveh.HasMesh("taillight-back-top-right")) availablelights = availablelights | TAILLIGHT_RIGHTTOP;
      if(_myveh.HasMesh("taillight-back-middle-left")) availablelights = availablelights | TAILLIGHT_LEFTMIDDLE;
      if(_myveh.HasMesh("taillight-back-middle-right")) availablelights = availablelights | TAILLIGHT_RIGHTMIDDLE;
      if(_myveh.HasMesh("taillight-back-bottom-left")) availablelights = availablelights | TAILLIGHT_LEFTBOTTOM;
      if(_myveh.HasMesh("taillight-back-bottom-right")) availablelights = availablelights | TAILLIGHT_RIGHTBOTTOM;
      _lightstatus = _lightstatus | (availablelights << 12) | TAILLIGHT_AVAILABLELOADED;
    }
    if(front) return (_lightstatus >> 18) & TAILLIGHT_LIGHTMASK;
    return (_lightstatus >> 12) & TAILLIGHT_LIGHTMASK;
  }

  int VCLTLDeterminingLigts(bool front)
  {
    Train mytrain = _myveh.GetMyTrain();
    Vehicle[] vehs = mytrain.GetVehicles();
    if((!mytrain.GetActiveDriver() and false /*Необходимость использования проверки наличия машиниста нужно обдумать*/) or !mytrain.GetFrontmostLocomotive() or (vehs[0] != me and vehs[vehs.size() - 1] != me) or
    (vehs[0] == me and ECLLogic.XOR(_myveh.GetDirectionRelativeToTrain(), front)) or (vehs[vehs.size() - 1] == me and !ECLLogic.XOR(_myveh.GetDirectionRelativeToTrain(), front))) return 0;
    int availablelights = VCLTLGetAvailableLights(front);
    if(ECLLogic.GetBitMask(availablelights, TAILLIGHT_LEFTTOP | TAILLIGHT_RIGHTTOP | TAILLIGHT_RIGHTMIDDLE)) return TAILLIGHT_LEFTTOP | TAILLIGHT_RIGHTTOP | TAILLIGHT_RIGHTMIDDLE;
    if(ECLLogic.GetBitMask(availablelights, TAILLIGHT_LEFTMIDDLE | TAILLIGHT_RIGHTMIDDLE)) return TAILLIGHT_LEFTMIDDLE | TAILLIGHT_RIGHTMIDDLE; 
    if(ECLLogic.GetBitMask(availablelights, TAILLIGHT_RIGHTMIDDLE)) return TAILLIGHT_RIGHTMIDDLE; 
    return 0;
  }                    
   
  void VCLTLLightsUpdated(bool front, int newLightsOn)
  {
    if(front) {
      _myveh.SetMeshVisible("taillight-front-top-left", newLightsOn & TAILLIGHT_LEFTTOP, VCLTLGetTailLightDuration(true, TAILLIGHT_LEFTTOP));
      _myveh.SetMeshVisible("taillight-front-top-right", newLightsOn & TAILLIGHT_RIGHTTOP, VCLTLGetTailLightDuration(true, TAILLIGHT_RIGHTTOP));
      _myveh.SetMeshVisible("taillight-front-middle-left", newLightsOn & TAILLIGHT_LEFTMIDDLE, VCLTLGetTailLightDuration(true, TAILLIGHT_LEFTMIDDLE));
      _myveh.SetMeshVisible("taillight-front-middle-right", newLightsOn & TAILLIGHT_RIGHTMIDDLE, VCLTLGetTailLightDuration(true, TAILLIGHT_RIGHTMIDDLE));
      _myveh.SetMeshVisible("taillight-front-bottom-left", newLightsOn & TAILLIGHT_LEFTBOTTOM, VCLTLGetTailLightDuration(true, TAILLIGHT_LEFTBOTTOM));
      _myveh.SetMeshVisible("taillight-front-bottom-right", newLightsOn & TAILLIGHT_RIGHTBOTTOM, VCLTLGetTailLightDuration(true, TAILLIGHT_RIGHTBOTTOM));
    } else {
      _myveh.SetMeshVisible("taillight-back-top-left", newLightsOn & TAILLIGHT_LEFTTOP, VCLTLGetTailLightDuration(false, TAILLIGHT_LEFTTOP));
      _myveh.SetMeshVisible("taillight-back-top-right", newLightsOn & TAILLIGHT_RIGHTTOP, VCLTLGetTailLightDuration(false, TAILLIGHT_RIGHTTOP));
      _myveh.SetMeshVisible("taillight-back-middle-left", newLightsOn & TAILLIGHT_LEFTMIDDLE, VCLTLGetTailLightDuration(false, TAILLIGHT_LEFTMIDDLE));
      _myveh.SetMeshVisible("taillight-back-middle-right", newLightsOn & TAILLIGHT_RIGHTMIDDLE, VCLTLGetTailLightDuration(false, TAILLIGHT_RIGHTMIDDLE));
      _myveh.SetMeshVisible("taillight-back-bottom-left", newLightsOn & TAILLIGHT_LEFTBOTTOM, VCLTLGetTailLightDuration(false, TAILLIGHT_LEFTBOTTOM));
      _myveh.SetMeshVisible("taillight-back-bottom-right", newLightsOn & TAILLIGHT_RIGHTBOTTOM, VCLTLGetTailLightDuration(false, TAILLIGHT_RIGHTBOTTOM));
    }
  }
  
  float VCLTLGetTailLightDuration(bool front, int light) 
  { 
    return 0.0; 
  }
  
  mandatory Soup VCLTLGetProperties(void)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("TailLightStatus", _lightstatus); 
    return soup;
  }

  mandatory void VCLTLSetProperties(Soup soup)
  {
    _lightstatus = soup.GetNamedTagAsInt("TailLightStatus", _lightstatus);
    int fronlight = VCLTLGetLightStatus(true); 
    int backlight = VCLTLGetLightStatus(false); 
    if(fronlight != 0) VCLTLLightsUpdated(true, fronlight);
    if(backlight != 0) VCLTLLightsUpdated(false, backlight);
    VCLTLUpdateTailLight();
  }
  
  public final void VCLTLCoupleChanged(void)
  {
    VCLTLUpdateTailLight();
    Vehicle[] vehs = _myveh.GetMyTrain().GetVehicles();
    VCL_TailLights first = cast<VCL_TailLights>((object)vehs[0]);
    VCL_TailLights last = cast<VCL_TailLights>((object)vehs[vehs.size() - 1]);
    if(first and first != me) first.VCLTLUpdateTailLight();
    if(last and last != first and last != me) last.VCLTLUpdateTailLight();
  }

};


//=============================================================================
// Name: VCL_VehicleWithTurnbuckle
// Desc: Класс, реализующий анимацию тормозной арматуры в зависимости от 
//       давления в ТЦ
// Note: Это класс является абстрактным и для работы должен быть унаследован.
//       Класс наследник должен наследоваться от Vehicle
//=============================================================================
class VCL_BrakeFixture
{

  final void VCLBFInit(void);
  
  public final Vehicle VCLBFGetMyVehicle(void);
  
  bool VCLBFGetBrakeFixtureState(void);
  
  void VCLBFUpdateBrakeFixture(bool braked); 



	//
	// РЕАЛИЗАЦИЯ
	//
  
  Vehicle m_myveh;
  VCL_Core m_core;
  bool m_state;

  final void VCLBFInit(void)
  {
    if(m_myveh) { Interface.Exception("BrakeFixture aready initialized"); return; }
    if(!me.isclass(Vehicle)) { Interface.Exception("This class must be inherited of Vehicle class"); return; }                             
    m_myveh = cast<Vehicle>((object)me);
    m_core = cast<VCL_Core>(TrainzScript.GetLibrary(m_myveh.GetAsset().LookupKUIDTable("vehicle-class-library")));
    m_core.VCLCAddBrakeFixture(me);
  }

  public final Vehicle VCLBFGetMyVehicle(void) { return m_myveh; }

  final float VCLBFBrakeCylinderPresure(void) { return (98101.7 * (m_myveh.GetEngineParam("brake-cylinder-pressure") - 0.00103341)) * 0.01; }

  bool VCLBFGetBrakeFixtureState(void) { return VCLBFBrakeCylinderPresure() >= 0.2; }

  void VCLBFUpdateBrakeFixture(bool braked)
  {
    if(m_myveh.HasMesh("brake-fixture")) m_myveh.SetMeshAnimationState("brake-fixture", braked);
    Bogey[] bogeys = m_myveh.GetBogeyList();
    if(bogeys) {
      int i;
      for(i = 0; i < bogeys.size(); ++i)
        if(bogeys[i].HasMesh("brake-fixture")) 
          bogeys[i].SetMeshAnimationState("brake-fixture", braked);
    }
  }
  
  public final void VCLBFUpdateBrakeFixture(void)
  {
    if(m_myveh){
      bool state = VCLBFGetBrakeFixtureState();
      if(state != m_state) {
        m_state = state;
        VCLBFUpdateBrakeFixture(state);
      }
    }
  }

}; 