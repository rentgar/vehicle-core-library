//=============================================================================
// File: vcl_eurowagon
// Desc: Содержит стэк базовых классов для европейских вагонов
// Auth: Алексей 'Эрендир' Зверев 2019 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "vcl_base.gs"
include "vehicle.gs"




class VCL_EuroWagonBase isclass VCL_VehicleWithTurnbuckle, VCL_TailLights, VCL_BrakeFixture
{
  

  




 /*

  void OnCoupleChange(CoupleArgs args)
  {
    UpdateCoupler(args);  //Обновление состояния винтовой стяжки

    //VCLTLUpdateTailLight();

    
    if(args.IsCoupled()) { //Если произошла сцепка
      if(args.Side() == SIDE_FRONT) m_myveh.GetFXAttachment("brakepipe-front").SetMesh("brakepipe-coupled", 0.0);
      else m_myveh.GetFXAttachment("brakepipe-back").SetMesh("brakepipe-coupled", 0.0);
    } else { //Если расцепились 
      if(args.Side() == SIDE_FRONT) m_myveh.GetFXAttachment("brakepipe-front").SetMesh("brakepipe-decoupled", 0.0);
      else m_myveh.GetFXAttachment("brakepipe-back").SetMesh("brakepipe-decoupled", 0.0);
    } 
  }
   */


};

class VCL_EuroWagon isclass Vehicle, VCL_EuroWagonBase
{


  public void Init(Asset asset)
  {
    inherited(asset);
    VCLCIInit();
    
    //InitCoupleProvider();
    VCLTLInit();
    VCLBFInit();
  }
  
};