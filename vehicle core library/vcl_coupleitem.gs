//=============================================================================
// File: vcl_coupleitem.gs
// Desc: Содержит основной базовый класс для реализации событий сцепки
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "vcl_base.gs"
include "vehicle.gs"

//=============================================================================
// Name: VCL_CoupleItem
// Desc: Базовый класс для обработки событий сцепки/расцепки
// Note: Для реализации, класс должен быть унаследован совместно с Vehicle. 
//       В Init(Asset) реализующега класса необходимо вызвать функцию
//       VCLCIInit().
//       В kuid-table реализующего актива подвижной единицы необходимо указать
//       запись для библиотеки Vehicle core library. Имя записи
//       vehicle-class-library. Внимание, указывайте последнею версию KUID
//       для библиотеки в записи.
//       Пример:
//       kuid-table
//       {
//         vehicle-class-library                 <kuid:151055:60042>
//       }        
//
//       Настройка может быть выаолнена через секцию vcl-coupleitem-settings 
//       вложенную в секуию extensions файла condif.txt реализующего актива.
//       Cтруктурв:
//       extensions
//       {
//         vcl-coupleitem-settings
//         {
//           disable-front-sound                 0
//           disable-rear-sound                  1
//           front-sound-asset-entryname         "soundlibrary"
//           rear-sound-asset-entryname          "soundlibrary"
//           front-couplesound-filename          "couple.wav"
//           rear-couplesound-filename           "couple.wav"
//           front-decouplesound-filename        "decouple.wav"
//           rear-decouplesound-filename         "decouple.wav"
//         }
//       }
//       Все теги конфигурации являются необязательными
//       
//       disable-front-sound
//         Значение 1 (true), если нужно отключить звуки сцепки/расцепки 
//         спереди вагона; или 0 (false), чтобы звук сцепки/расцепки 
//         присутствовал/
//       Значение по умолчанию: 0
//
//       disable-rear-sound
//         Значение 1 (true), если нужно отключить звуки сцепки/расцепки 
//         сзади вагона; или 0 (false), чтобы звук сцепки/расцепки 
//         присутствовал/
//       Значение по умолчанию: 0
//
//       front-sound-asset-entryname
//         Имя записи в kuid-table содержащей KUID для актива со звуками 
//         сцепки/расцепки спереди вагона.
//       Значение по умолчанию: 
//       Если данный тэг не указан или имеет пустое значение (""), то звуки
//       будут использованы из библиотеки "Vehicle core library"
//
//       rear-sound-asset-entryname
//         Имя записи в kuid-table содержащей KUID для актива со звуками 
//         сцепки/расцепки спереди вагона.
//       Значение по умолчанию: 
//       Если данный тэг не указан или имеет пустое значение (""), то звуки
//       будут использованы из библиотеки "Vehicle core library"
//
//       front-couplesound-filename
//         Имя файла звука сцепки в активе звуков спереди вагона.
//       Значение по умолчанию:
//       Тэг является обязательным при заданном тэге 
//       front-sound-asset-entryname. Если библиотека звуков в тэге 
//       front-sound-asset-entryname не задана, то значение тэга
//       front-couplesound-filename будет игнорировано 
//
//       front-decouplesound-filename
//         Имя файла звука расцепки в активе звуков спереди вагона.
//       Значение по умолчанию:
//       Тэг является обязательным при заданном тэге 
//       front-sound-asset-entryname. Если библиотека звуков в тэге 
//       front-sound-asset-entryname не задана, то значение тэга
//       front-decouplesound-filenamee будет игнорировано 
//
//       rear-couplesound-filename
//         Имя файла звука сцепки в активе звуков сзади вагона.
//       Значение по умолчанию:
//       Тэг является обязательным при заданном тэге 
//       rear-sound-asset-entryname. Если библиотека звуков в тэге 
//      rear-sound-asset-entryname не задана, то значение тэга
//       rear-couplesound-filename будет игнорировано 
//
//       front-decouplesound-filename
//         Имя файла звука расцепки в активе звуков спереди вагона.
//       Значение по умолчанию:
//       Тэг является обязательным при заданном тэге 
//       rear-sound-asset-entryname. Если библиотека звуков в тэге 
//       rear-sound-asset-entryname не задана, то значение тэга
//       rear-decouplesound-filename будет игнорировано
//
//       Для установки своих звуков для сцепки/расцепки спереди вагона
//       необходимо, что бы тэг disable-front-sound имел значение 0 (false),
//       теги front-sound-asset-entryname, front-couplesound-filename,
//       front-decouplesound-filename были заданы, а запись с именем заданным
//       в тэге front-sound-asset-entryname присутствовала в kuid-table.
//
//       Для установки своих звуков для сцепки/расцепки спереди вагона
//       необходимо, что бы тэг disable-rear-sound имел значение 0 (false),
//       теги rear-sound-asset-entryname, rear-couplesound-filename,
//       rear-decouplesound-filename были заданы, а запись с именем заданным
//       в тэге rear-sound-asset-entryname присутствовала в kuid-table.
//=============================================================================
class VCL_CoupleItem
{

  //=============================================================================
  // Name: COUPLEEVENT_*
  // Desc: Набор флагов, определяющих, произошедшее событие стцепки
  //=============================================================================
  define int COUPLEEVENT_INIT                       = 1 << 0;       //Инициализация (указывает, что событие произошло в следствии инициализации при старте сессии)
  define int COUPLEEVENT_COUPLE                     = 1 << 1;       //Была выполнена сцепка 
  define int COUPLEEVENT_DECOUPLE                   = 1 << 2;       //Была выполнена расцепка
  
  //=============================================================================
  // Name: COUPLESIDE_*
  // Desc: Набор констант, определяющих, c какой стороны вагона произошло событие
  //=============================================================================
  public define int COUPLESIDE_FRONT                = 1;            //Собьтие произошло с передней части вагона/локомотива
  public define int COUPLESIDE_REAR                 = 2;            //Событие произогло с тыльной стороны вагона/локомтива

  //=============================================================================
  // Name: VCLCIInit
  // Desc: Выполняет начальную инициализацию класса.
  // Note: Необходимо обязательно вызывать в функции void Init(Asset) класса
  //       наследника. Класс наследник должен наследоваться от Vehicle
  //=============================================================================
  mandatory void VCLCIInit(void);
  
  //=============================================================================
  // Name: VCLCIOnCoupleEvent
  // Desc: Событие сцепки/расцепки
  // Parm: event — Битовая маска флагов COUPLEEVENT_*, определяющая возникшее
  //       событие. Внимание! Проверку нужно производить по биту маски, например
  //       (event & COUPLEEVENT_COUPLE) для события сцепки и
  //       (event & COUPLEEVENT_DECOUPLE) для события расцеки соответственно.   
  // Parm: side — Значение одной из констант COUPLESIDE_*, определяющих с какой
  //       стороны вагона/локомотива возникло событие сцепки/расцепки. 
  // Parm: neighbour — Соседний вагон/локомтив, с которым выполнена сцпка, или
  //       значение null, если выполнена расцепка
  // Note: Для реализации обработчика события необходимо переопределить в
  //       наследуемом классе.
  //       Внимание! Данная функция вызывается автоматически при возникновении
  //       события сцепки/расцепки. Не вызывайте её из стороннего скрипта.                      
  //=============================================================================
  void VCLCIOnCoupleEvent(int event, int side, Vehicle neighbour){}
  
  //=============================================================================
  // Name: VCLCIIsCoupled
  // Desc: Возвращает значение true, если с указанной стороны сцепка выполнена;
  //       в противном случае — значение false.
  // Parm: side — Значение одной из констант COUPLESIDE_*, определяющих с какой
  //       стороны вагона/локомотива нужно выполнить проверку наличия сцепления. 
  //=============================================================================
  public final bool VCLCIIsCoupled(int side);
  
  //=============================================================================
  // Name: VCLCIIsCoupled
  // Desc: Возвращает слсженюю подвижную еденицу, с которой выполнена сцепка
  //       с указанной стороны вагона/локомтива.
  // Parm: side — Значение одной из констант COUPLESIDE_*, определяющих с какой
  //       стороны вагона/локомотива нужно выполнить поиск соседней подвижной
  //       еденицы.
  // Retn: Экземпляр класса Vehicle, представляющий соседнюю подвижную единицу
  //       с указанной стороны; или значение null, если с указанной стороы
  //       сцепка не выполнена.
  //=============================================================================
  public final Vehicle VCLCIGetNeighbor(int side); 
  
	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int COUPLECFG_INITED           = 1 << 0;   //Сцепка инициализирована
  define int COUPLECFG_FRONTNOSOUND     = 1 << 1;   //Указатель, что звук сцепки/расцепки спереди вагона отсутствует
  define int COUPLECFG_REARNOSOUND      = 1 << 2;   //Указатель, что звук сцепки/расцепки сздаи вагона отсутствует
  
  Vehicle _myveh;                                   //Текущая подвижная единица
  Vehicle _frontveh;                                //Соседняя подвижная единица спереди
  Vehicle _rearveh;                                 //Соседняя подвижная единица сзади
  
  int _config;                                      //Конфигурация обработчика
  
  Asset _frontcouplesound;                          //Актив, содержащий звуки сцепки/расцепки спереди
  Asset _rearcouplesound;                           //Актив, содержащий звуки сцепки/расцепки спереди
  string _frontcouplesoundfile;                     //Имя файла звука сцепки спереди
  string _frontdecouplesoundfile;                   //Имя файла звука расцепки спереди
  string _rearcouplesoundfile;                      //Имя файла звука сцепки сзади
  string _reardecouplesoundfile;                    //Имя файла звука расцепки сзади
  
  public final bool VCLCIIsInited(void)
  {
    return  (bool)(_config & COUPLECFG_INITED);
  }
  
  public final void VCLCISetInitedState(void)
  {
    _config = _config | COUPLECFG_INITED;
  }
  
  public final bool VCLCIIsCoupled(int side) 
  {
    if(!_myveh) {
      Interface.Exception("VCL_CoupleItem.VCLCIIsCoupled> invalid call to VCLCIIsCoupled(int) prior to call VCLCIInit()");
      return false;
    }
    if(side != COUPLESIDE_FRONT and side != COUPLESIDE_REAR) {
      _myveh.Exception("VCL_CoupleItem.VCLCIIsCoupled> value of 'side' argument must be value of one of COUPLESIDE_* constants");
      return false;
    }
    return (side == COUPLESIDE_FRONT and _frontveh) or (side == COUPLESIDE_REAR and _rearveh);
  }

  public final Vehicle VCLCIGetNeighbor(int side)
  {
    if(!_myveh) {
      Interface.Exception("VCL_CoupleItem.VCLCIGetNeighbor> invalid call to VCLCIGetNeighbor(int) prior to call VCLCIInit()");
      return null;
    }
    if(side != COUPLESIDE_FRONT and side != COUPLESIDE_REAR) {
      _myveh.Exception("VCL_CoupleItem.VCLCIGetNeighbor> value of 'side' argument must be value of one of COUPLESIDE_* constants");
      return null;
    }
    if(side != COUPLESIDE_FRONT) return _frontveh;
    return _rearveh;
  }
  
  public final void VCLCIInitState(bool front, Vehicle neighbour)
  {
    if(!(_config & COUPLECFG_INITED)) {
      int event = COUPLEEVENT_INIT, side;
      if(neighbour) event = event | COUPLEEVENT_COUPLE;
      else event = event | COUPLEEVENT_DECOUPLE;   
      if(front) { 
        _frontveh = neighbour;
        side = COUPLESIDE_FRONT; 
      } else {
        _rearveh = neighbour;
        side = COUPLESIDE_REAR; 
      }
      VCLCIOnCoupleEvent(event, side, neighbour);     
    }
  } 
 
  final void VCLCICoupleHandler(Message msg)
  {
    Vehicle neighbour = msg.src;
    bool coupled = msg.minor == "Coupled";
    if(_myveh and neighbour and msg.dst == me) {
      int side = 0;
      if(coupled) {
        side = _myveh.GetCouplingDirection(neighbour);
        if(side == Vehicle.DIRECTION_BACKWARD) side = COUPLESIDE_REAR;
        if(side == COUPLESIDE_FRONT) _frontveh = neighbour; 
        else if(side == COUPLESIDE_REAR) _rearveh = neighbour; 
      } else {
        if(_frontveh == neighbour) {
          side = COUPLESIDE_FRONT;
          _frontveh = null;
        } else if(_rearveh == neighbour) {
          side = COUPLESIDE_REAR;
          _rearveh = null;
        }
      }
      if(side != 0) {
        if(side == COUPLESIDE_FRONT and !(_config & COUPLECFG_FRONTNOSOUND)) {
          if(coupled) World.PlaySound(_frontcouplesound, _frontcouplesoundfile, 1.0, 15.0, 50.0, _myveh, "a.limfront");
          else World.PlaySound(_frontcouplesound, _frontdecouplesoundfile, 1.0, 15.0, 50.0, _myveh, "a.limfront");
        } 
        if(side == COUPLESIDE_REAR and !(_config & COUPLECFG_REARNOSOUND)) {
          if(coupled) World.PlaySound(_rearcouplesound, _rearcouplesoundfile, 1.0, 15.0, 50.0, _myveh, "a.limback");
          else World.PlaySound(_rearcouplesound, _reardecouplesoundfile, 1.0, 15.0, 50.0, _myveh, "a.limback");
        }
        
        


        if(!coupled) neighbour = null;        
        if(coupled) VCLCIOnCoupleEvent(COUPLEEVENT_COUPLE, side, neighbour); 
        else VCLCIOnCoupleEvent(COUPLEEVENT_DECOUPLE, side, neighbour); 
        
      
        
      
        
        //Тут вставить вызов обработчиков для других реализующих классов 

      
      }
      VCL_TailLights taillights = cast<VCL_TailLights>((object)me);
      
      if(taillights) taillights.VCLTLCoupleChanged();
      
    }
  } 
  
  
  mandatory void VCLCIInit(void)
  {
    if(_myveh) return;
    _myveh = cast<Vehicle>((object)me);
    if(!_myveh){
      Interface.Exception("VCL_CoupleItem.VCLCIInit> implementing class is not an inheritor of Vehicle class");
      return;
    }
    Asset libraryasset = _myveh.GetAsset().FindAsset("vehicle-class-library");
    if(!libraryasset) {
      _myveh.Exception("VCL_CoupleItem.VCLCIInit> 'vehicle-class-library' entry in kuid-table for library asset in '" + _myveh.GetAsset().GetLocalisedName() + "' asset was not found.");
      return;
    }
    _frontcouplesound = libraryasset;
    _rearcouplesound = libraryasset;
    _frontcouplesoundfile = "sounds/couple.wav";                     
    _frontdecouplesoundfile = "sounds/decouple.wav";                   
    _rearcouplesoundfile = "sounds/couple.wav";                        
    _reardecouplesoundfile = "sounds/decouple.wav";
    Soup configuration = _myveh.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("vcl-coupleitem-settings");
    if(configuration.GetNamedTagAsBool("disable-front-sound")) _config = _config | COUPLECFG_FRONTNOSOUND; 
    if(configuration.GetNamedTagAsBool("disable-rear-sound")) _config = _config | COUPLECFG_REARNOSOUND;
    if(!( _config & COUPLECFG_FRONTNOSOUND)) {
      string soundassetname = configuration.GetNamedTag("front-sound-asset-entryname");
      string couplefile = configuration.GetNamedTag("front-couplesound-filename"); 
      string decouplefile = configuration.GetNamedTag("front-decouplesound-filename");
      if(soundassetname != "" and couplefile != "" and decouplefile != "") {
        Asset soundasset = _myveh.GetAsset().FindAsset(soundassetname);
        if(soundasset) {
          _frontcouplesound = soundasset;
          _frontcouplesoundfile = couplefile; 
          _frontdecouplesoundfile = decouplefile; 
        }
      } 
    }
    if(!( _config & COUPLECFG_REARNOSOUND)) {
      string soundassetname = configuration.GetNamedTag("rear-sound-asset-entryname");
      string couplefile = configuration.GetNamedTag("rear-couplesound-filename"); 
      string decouplefile = configuration.GetNamedTag("rear-decouplesound-filename");
      if(soundassetname != "" and couplefile != "" and decouplefile != "") {
        Asset soundasset = _myveh.GetAsset().FindAsset(soundassetname);
        if(soundasset) {
          _rearcouplesound = soundasset;
          _rearcouplesoundfile = couplefile; 
          _reardecouplesoundfile = decouplefile; 
        }
      } 
    }
    _myveh.AddHandler(_myveh, "Vehicle", "Coupled", "VCLCICoupleHandler");
    _myveh.AddHandler(_myveh, "Vehicle", "Decoupled", "VCLCICoupleHandler");
    TrainzScript.GetLibrary(libraryasset.GetKUID());
  }

};
